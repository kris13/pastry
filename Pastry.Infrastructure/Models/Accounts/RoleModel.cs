﻿namespace Pastry.Infrastructure.Models.Accounts
{
    /// <summary>
    /// Defines the model of a role.
    /// </summary>
    public sealed class RoleModel
    {
        /// <summary>
        /// Id of the role.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The name of the role.
        /// </summary>
        public string Name { get; set; }
    }
}