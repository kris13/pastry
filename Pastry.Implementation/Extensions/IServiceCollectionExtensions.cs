﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Pastry.Data;
using Pastry.Implementation.Services.Accounts;
using Pastry.Implementation.Services.Products;
using Pastry.Infrastructure.Models.Accounts;
using Pastry.Infrastructure.Services.Accounts;
using Pastry.Infrastructure.Services.Products;

namespace Pastry.Implementation.Extensions
{
    /// <summary>
    /// Extension methods for <see cref="IServiceCollection"/>.
    /// </summary>
    public static class IServiceCollectionExtensions
    {
        /// <summary>
        /// Adds the application services to the specified <see cref="IServiceCollection"/>.
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/> to add services to.</param>
        /// <returns>The <see cref="IServiceCollection"/> so that additional calls can be chained.</returns>
        public static IServiceCollection AddApplicationServices(this IServiceCollection services)
        {
            services.AddDbContext<PastryDbContext>(opt => opt.UseInMemoryDatabase("Test"));

            services.AddTransient<IProductService, ProductService>();

            services.AddTransient<IRoleStoreService, RoleStoreService>();
            services.AddTransient<IUserStoreService, UserStoreService>();

            services.AddTransient<IRoleStore<RoleModel>>((serviceProvider) => serviceProvider.GetRequiredService<IRoleStoreService>());
            services.AddTransient<IUserStore<UserModel>>((serviceProvider) => serviceProvider.GetRequiredService<IUserStoreService>());

            return services;
        }
    }
}