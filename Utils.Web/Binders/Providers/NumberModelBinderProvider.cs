﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Linq;
using Utils.Guards;

namespace Utils.Web.Binders.Providers
{
    /// <summary>
    /// An <see cref="IModelBinderProvider"/> for binding number.
    /// </summary>
    public sealed class NumberModelBinderProvider : IModelBinderProvider
    {
        private static readonly Type[] _numberTypes = new[] { typeof(decimal), typeof(double), typeof(float) };

        /// <inheritdoc/>
        public IModelBinder GetBinder(ModelBinderProviderContext context)
        {
            Guard.NotNull(nameof(context), context);

            if (_numberTypes.Contains(context.Metadata.UnderlyingOrModelType))
            {
                return new NumberBinder();
            }

            return null;
        }
    }
}