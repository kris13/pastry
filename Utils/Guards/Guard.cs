﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using Utils.Exceptions;
using Utils.Extensions;

namespace Utils.Guards
{
    /// <summary>
    /// Provides guard clauses.
    /// </summary>
    [DebuggerStepThrough]
    public static class Guard
    {
        private static class Messages
        {
            public static string NotNull(string parameterName) => $"{parameterName} cannot be null.";

            public static string ObjectInvalid(string parameterName) => $"{parameterName} is not valid.";
        }

        /// <summary>
        /// Guards against a null parameter.
        /// </summary>
        /// <typeparam name="T">The type of the parameter value.</typeparam>
        /// <param name="parameterName">The name of the parameter.</param>
        /// <param name="parameterValue">The value of the parameter.</param>
        public static void NotNull<T>(string parameterName, T parameterValue)
        {
            if (typeof(T).IsNullable() && parameterValue == null)
            {
                throw new ArgumentNullException(Messages.NotNull(parameterName), parameterName);
            }
        }

        /// <summary>
        /// Guards against the properties of an object are invalid (use the DataAnnotations).
        /// </summary>
        /// <typeparam name="T">The type of the <paramref name="parameterValue"/>.</typeparam>
        /// <param name="parameterName">The name of the parameter.</param>
        /// <param name="parameterValue">The value of the parameter.</param>
        /// <exception cref="InvalidObjectException"></exception>
        /// <remarks>Validate also that the <paramref name="parameterValue"/> is not null.</remarks>
        public static void InvalidProperties<T>(string parameterName, T parameterValue) where T : class
        {
            NotNull(parameterName, parameterValue);

            var context = new ValidationContext(parameterValue, serviceProvider: null, items: null);
            var validationResults = new List<ValidationResult>();

            if (!Validator.TryValidateObject(parameterValue, context, validationResults, true))
            {
                throw new InvalidObjectException(Messages.ObjectInvalid(parameterName), validationResults);
            }
        }
    }
}