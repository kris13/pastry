﻿namespace Pastry.Infrastructure.Models.Accounts
{
    /// <summary>
    /// Defines the model of a user account.
    /// </summary>
    public sealed class UserModel
    {
        /// <summary>
        /// Id of the user.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The name of the user.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// The email of the user.
        /// </summary>
        public string Email { get; set; }
    }
}