﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.ComponentModel;
using System.Threading.Tasks;
using Utils.Guards;

namespace Utils.Web.Binders
{
    /// <summary>
    /// An <see cref="IModelBinder"/> for number.
    /// </summary>
    public sealed class NumberBinder : IModelBinder
    {
        /// <inheritdoc/>
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            Guard.NotNull(nameof(bindingContext), bindingContext);

            var valueProviderResult = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            if (!string.IsNullOrEmpty(valueProviderResult.FirstValue))
            {
                var converter = TypeDescriptor.GetConverter(bindingContext.ModelType);
                try
                {
                    var value = converter.ConvertFromInvariantString(valueProviderResult.FirstValue);
                    bindingContext.Result = ModelBindingResult.Success(value);
                }
                catch
                {
                    var errorMessage = bindingContext
                                          .ModelMetadata
                                          .ModelBindingMessageProvider
                                          .AttemptedValueIsInvalidAccessor(valueProviderResult.ToString(), bindingContext.ModelType.Name);

                    bindingContext
                        .ModelState
                        .TryAddModelError(bindingContext.ModelName, errorMessage);
                }
            }

            return Task.CompletedTask;
        }
    }
}