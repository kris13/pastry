﻿using Microsoft.EntityFrameworkCore;
using Pastry.Data;
using Pastry.Data.Entities.Products;
using Pastry.Implementation.Helpers;
using Pastry.Infrastructure.Models.Products;
using Pastry.Infrastructure.Services.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utils.Guards;

namespace Pastry.Implementation.Services.Products
{
    /// <inheritdoc/>
    public class ProductService : IProductService
    {
        private readonly PastryDbContext _dbContext;
        private readonly DateTime _now = DateTime.Now.Date;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProductService"/> class.
        /// </summary>
        public ProductService(PastryDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        /// <inheritdoc/>
        public async Task<ICollection<ProductModel>> GetAllAsync()
        {
            var eDiscounts = await _dbContext.DiscountPrices.ToListAsync();
            var entities = await _dbContext.Products
                .Include(e => e.Ingredients)
                .ThenInclude(e => e.Ingredient)
                .AsNoTracking()
                .ToListAsync();

            return ProductHelper.MapToModels(entities, m => AdjustAvailability(m, _now, eDiscounts));
        }

        /// <inheritdoc/>
        public async Task<ICollection<ProductItemModel>> GetAllItemsAsync()
        {
            var eDiscounts = await _dbContext.DiscountPrices.ToListAsync();
            var entities = await _dbContext.Products
                .AsNoTracking()
                .ToListAsync();

            return ProductHelper.MapToItemModels(entities, m => AdjustAvailability(m, _now, eDiscounts));
        }

        /// <inheritdoc/>
        public async Task<ICollection<IngredientModel>> GetAllIngredientsAsync()
        {
            var entities = await _dbContext.Ingredients
                .OrderBy(e => e.Name)
                .AsNoTracking()
                .ToListAsync();

            return ProductHelper.MapToModels(entities);
        }

        /// <inheritdoc/>
        public async Task<ProductCrudModel> FindByIdAsync(int id)
        {
            var eDiscounts = await _dbContext.DiscountPrices.ToListAsync();
            var entity = await _dbContext.Products
                .Include(e => e.Ingredients)
                .ThenInclude(e => e.Ingredient)
                .AsNoTracking()
                .FirstOrDefaultAsync(e => e.Id == id);

            return entity != null ? ProductHelper.MapToModel(entity) : null;
        }

        /// <inheritdoc/>
        public Task DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc/>
        public async Task<bool> CreateAsync(ProductCrudModel model)
        {
            Guard.NotNull(nameof(model), model);
            Guard.InvalidProperties(nameof(model), model);

            var entity = ProductHelper.MapToEntity(model);

            await _dbContext.Products.AddAsync(entity);

            var result = await _dbContext.SaveChangesAsync();

            model.Id = entity.Id;

            return result > 0;
        }

        /// <inheritdoc/>
        public async Task<bool> UpdateAsync(ProductCrudModel model)
        {
            Guard.NotNull(nameof(model), model);
            Guard.InvalidProperties(nameof(model), model);

            var result = false;
            var entity = await _dbContext.Products
                .Include(e => e.Ingredients)
                .FirstOrDefaultAsync(e => e.Id == model.Id);

            if (entity != null)
            {
                ProductHelper.MapToEntity(model, entity);

                _dbContext.Products.Update(entity);

                result = (await _dbContext.SaveChangesAsync()) > 0;
            }

            return result;
        }

        #region Privates methods

        private void AdjustAvailability(ISoldModel model, DateTime now, ICollection<DiscountPriceEntity> eDiscountPrices)
        {
            var days = (now - model.SaleDate).TotalDays + 1;
            var eDiscountPrice = eDiscountPrices
                .Where(e => e.Days <= days)
                .OrderByDescending(e => e.Days)
                .FirstOrDefault();

            if (eDiscountPrice != null)
            {
                model.DiscountedPrice = model.Price - Math.Round((decimal)eDiscountPrice.Percent * model.Price, 2);
                if (model.DiscountedPrice == 0)
                {
                    model.AvailableQuantity = 0;
                }
            }
        }

        #endregion Privates methods
    }
}