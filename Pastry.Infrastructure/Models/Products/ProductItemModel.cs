﻿using System;

namespace Pastry.Infrastructure.Models.Products
{
    /// <summary>
    /// Defines the properties of a product item.
    /// </summary>
    public sealed class ProductItemModel : ISoldModel
    {
        /// <summary>
        /// Id of the product.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Name of the product.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Quantity of the product.
        /// </summary>
        public uint Quantity { get; set; }

        /// <summary>
        /// Available quantity of the product.
        /// </summary>
        public uint AvailableQuantity { get; set; }

        /// <summary>
        /// Price  of the product.
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Date of sale of the product.
        /// </summary>
        public DateTime SaleDate { get; set; }

        /// <summary>
        /// Discounted price of the product.
        /// </summary>
        public decimal? DiscountedPrice { get; set; }
    }
}