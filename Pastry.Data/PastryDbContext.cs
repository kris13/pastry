﻿using Microsoft.EntityFrameworkCore;
using Pastry.Data.Entities.Account;
using Pastry.Data.Entities.Products;

namespace Pastry.Data
{
    public sealed class PastryDbContext : DbContext
    {
        public DbSet<UserEntity> Users { get; set; }

        public DbSet<IngredientEntity> Ingredients { get; set; }

        public DbSet<ProductEntity> Products { get; set; }

        public DbSet<ProductIngredientEntity> ProductIngredients { get; set; }

        public DbSet<DiscountPriceEntity> DiscountPrices { get; set; }

        public PastryDbContext(DbContextOptions<PastryDbContext> options)
            : base(options)
        {
        }
    }
}