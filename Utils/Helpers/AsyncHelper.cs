﻿using System;
using System.Reflection;
using System.Threading.Tasks;

namespace Utils.Helpers
{
    /// <summary>
    /// Provides some helper methods to work with async methods.
    /// </summary>
    public static class AsyncHelper
    {
        /// <summary>
        /// Executes an async Task method which has a void return value synchronously.
        /// USAGE: AsyncUtil.RunSync(() => AsyncMethod());
        /// </summary>
        /// <param name="task">Task method to execute</param>
        public static void RunSync(Func<Task> task)
            => Task.Run(task)
                .Wait();

        /// <summary>
        /// Executes an async Task&lt;T&gt; method which has a T return type synchronously
        /// USAGE: T result = AsyncUtil.RunSync(() => AsyncMethod&lt;T&gt;());
        /// </summary>
        /// <typeparam name="T">Return Type</typeparam>
        /// <param name="task">Task&lt;T&gt; method to execute</param>
        /// <returns>The result of the method call.</returns>
        public static T RunSync<T>(Func<Task<T>> task)
            => task.Invoke().Result;

#pragma warning disable RCS1047

        /// <summary>
        /// Checks if given method is an async method.
        /// </summary>
        /// <param name="this">The @this to act on.</param>
        /// <returns>True if the method is an async operation; otherwise false.</returns>
        public static bool IsAsync(this MethodInfo @this)
        {
            return @this.ReturnType == typeof(Task) ||
                (@this.ReturnType.GetTypeInfo().IsGenericType && @this.ReturnType.GetGenericTypeDefinition() == typeof(Task<>))
            ;
        }

#pragma warning restore RCS1047
    }
}