﻿namespace Pastry.Web
{
    /// <summary>
    /// Defines the constants used by the application.
    /// </summary>
    public static class Constants
    {
        /// <summary>
        /// Defines the routes name constants.
        /// </summary>
        public static class Routes
        {
            /// <summary>
            /// Error page.
            /// </summary>
            public static string ERROR = "Error";

            /// <summary>
            /// Defines the routes name for the accounts.
            /// </summary>
            public static class Account
            {
                /// <summary>
                /// Login page.
                /// </summary>
                public const string LOGIN = "Account.Login";

                /// <summary>
                /// Logout page.
                /// </summary>
                public const string LOGOUT = "Account.Logout";
            }

            /// <summary>
            /// Defines the routes name for the shop.
            /// </summary>
            public static class Shop
            {
                /// <summary>
                /// Page for displaying the shop products.
                /// </summary>
                public const string PRODUCTS = "Shop.Products";
            }

            /// <summary>
            /// Defines the routes name for the product management.
            /// </summary>
            public static class ProductManagement
            {
                /// <summary>
                /// Page for displaying the products.
                /// </summary>
                public const string PRODUCTS = "ProductManagement.Products";

                /// <summary>
                /// Page for creating a product.
                /// </summary>
                public const string CREATE_PRODUCT = "ProductManagement.CreateProduct";

                /// <summary>
                /// Page for editing a product.
                /// </summary>
                public const string EDIT_PRODUCT = "ProductManagement.EditProduct";

                /// <summary>
                /// Action for saving a product.
                /// </summary>
                public const string SAVE_PRODUCT = "ProductManagement.SaveProduct";
            }

            /// <summary>
            /// Root page of the application.
            /// </summary>
            public const string ROOT = Shop.PRODUCTS;
        }
    }
}