﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pastry.Data.Entities.Account
{
    /// <summary>
    /// Defines the entity of a user account.
    /// </summary>
    public sealed class UserEntity
    {
        /// <summary>
        /// Id of the user.
        /// </summary>
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// The name of the user.
        /// </summary>
        [Required, MaxLength(64)]
        public string UserName { get; set; }

        /// <summary>
        /// The email of the user.
        /// </summary>
        [Required, MaxLength(128)]
        public string Email { get; set; }

        /// <summary>
        /// The password hash of the user.
        /// </summary>
        [Required]
        public string PasswordHash { get; set; }
    }
}