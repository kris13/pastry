﻿using Microsoft.AspNetCore.Mvc;

namespace Pastry.Web.Controllers
{
    /// <summary>
    /// <see cref="Controller"/> used to display the error pages.
    /// </summary>
    public sealed class ErrorController : Controller
    {
        /// <summary>
        /// Gets the HTML page for displaying the error page.
        /// </summary>
        /// <param name="code">The code of the error.</param>
        /// <returns>The HTML code for displaying the error page.</returns>
        [HttpGet]
        public ViewResult Index(int code)
        {
            return View(code);
        }
    }
}