﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;

namespace Utils.Web.Extensions
{
    /// <summary>
    /// Extension methods for <see cref="IServiceCollection"/>.
    /// </summary>
    public static class IServiceCollectionExtensions
    {
        /// <summary>
        /// Adds antiforgery services to the specified <see cref="IServiceCollection"/>.
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/> to add services to.</param>
        /// <returns>The <see cref="IServiceCollection"/> so that additional calls can be chained.</returns>
        public static IServiceCollection AddAntiforgeryService(this IServiceCollection services)
        {
            return services
                 .AddAntiforgery(options =>
                 {
                     options.Cookie.Name = "__antiforgery";
                     options.HeaderName = "__antiforgery";
                     options.Cookie.Path = "/";
                     options.Cookie.SecurePolicy = CookieSecurePolicy.SameAsRequest;
                 });
        }

        /// <summary>
        /// Adds <see cref="IUrlHelper"/> to the specified <see cref="IServiceCollection"/>.
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/> to add services to.</param>
        /// <returns>The <see cref="IServiceCollection"/> so that additional calls can be chained.</returns>
        public static IServiceCollection AddUrlHelper(this IServiceCollection services)
        {
            return services
                .AddSingleton<IActionContextAccessor, ActionContextAccessor>()
                .AddScoped(factory =>
                {
                    var actionContext = factory
                        .GetRequiredService<IActionContextAccessor>()
                        .ActionContext;

                    if (actionContext == null)
                    {
                        var httpContext = factory.GetRequiredService<IHttpContextAccessor>().HttpContext;
                        var routeData = httpContext.GetRouteData();
                        actionContext = new ActionContext(httpContext, routeData, new ActionDescriptor());
                    }

                    return factory
                            .GetRequiredService<IUrlHelperFactory>()
                            .GetUrlHelper(actionContext);
                });
        }
    }
}