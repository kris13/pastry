using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace Pastry.Web
{
    /// <summary>
    /// Main programm.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// Main methods called to start the application.
        /// </summary>
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        /// <summary>
        /// Creates a new instance of the <see cref="WebHostBuilder"/>.
        /// </summary>
        /// <returns>The <see cref="WebHostBuilder"/> created with the <see cref="Startup"/> class used by the WebHostBuilder</returns>
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => webBuilder.UseStartup<Startup>());
    }
}