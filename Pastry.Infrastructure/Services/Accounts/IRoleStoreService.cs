﻿using Microsoft.AspNetCore.Identity;
using Pastry.Infrastructure.Models.Accounts;

namespace Pastry.Infrastructure.Services.Accounts
{
    /// <summary>
    /// Provides the methods to manage the roles.
    /// </summary>
    public interface IRoleStoreService : IRoleStore<RoleModel>
    {
    }
}