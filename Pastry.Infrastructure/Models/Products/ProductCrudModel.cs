﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Pastry.Infrastructure.Models.Products
{
    /// <summary>
    /// Defines the properties of a product for the CRUD operations.
    /// </summary>
    public sealed class ProductCrudModel
    {
        /// <summary>
        /// Id of the product.
        /// </summary>
        [Required]
        public int Id { get; set; }

        /// <summary>
        /// Name of the product.
        /// </summary>
        [Display(Name = "Nome")]
        [Required, MaxLength(256)]
        public string Name { get; set; }

        /// <summary>
        /// Quantity of the product.
        /// </summary>
        [Display(Name = "Quantità")]
        [Required, Range(0, int.MaxValue)]
        public uint Quantity { get; set; }

        /// <summary>
        /// Price  of the product.
        /// </summary>
        [Required, Range(0, double.MaxValue)]
        [Display(Name = "Prezzo")]
        public decimal Price { get; set; }

        /// <summary>
        /// Date of sale of the product.
        /// </summary>
        [Required, DataType(DataType.Date)]
        [Display(Name = "Data di vendita")]
        public DateTime SaleDate { get; set; }

        /// <summary>
        /// Ingredients of the product.
        /// </summary>
        [Display(Name = "Nome")]
        public ICollection<ProductIngredientModel> Ingredients { get; set; }
    }
}