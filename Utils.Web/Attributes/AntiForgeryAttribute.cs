﻿using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using Utils.Guards;
using Utils.Helpers;

namespace Utils.Web.Attributes
{
    /// <summary>
    /// Specifies that the class or method that this attribute is applied validates the anti-forgery token.
    /// If the anti-forgery token is not available, or if the token is invalid, the validation will fail and an entry is added to the <see cref="ActionContext.ModelState"/>.
    /// </summary>
    public sealed class AntiForgeryAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AntiForgeryAttribute"/> class.
        /// </summary>
        public AntiForgeryAttribute()
        {
            Order = int.MinValue;
        }

        /// <inheritdoc />
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            Guard.NotNull(nameof(context), context);

            var antiforgery = context.HttpContext.RequestServices.GetRequiredService<IAntiforgery>();
            var isRequestValid = AsyncHelper.RunSync(() => antiforgery.IsRequestValidAsync(context.HttpContext));
            if (!isRequestValid)
            {
                context.ModelState.AddModelError(string.Empty, "AntiForgery invalid.");
            }

            base.OnActionExecuting(context);
        }
    }
}