﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Pastry.Infrastructure.Models.Accounts;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Utils.Web.Attributes;

namespace Pastry.Web.Areas.Cms.Controllers
{
    /// <summary>
    /// <see cref="Controller"/> used for the user login/logout.
    /// </summary>
    public sealed class AccountController : Controller
    {
        private readonly SignInManager<UserModel> _signInManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountController"/> class.
        /// </summary>
        /// <param name="signInManager">The <see cref="SignInManager{TUser}"/> to use.</param>
        public AccountController(SignInManager<UserModel> signInManager)
        {
            _signInManager = signInManager;
        }

        /// <summary>
        /// Gets the "login" page view.
        /// </summary>
        [HttpGet]
        public async Task<IActionResult> Login()
        {
            if (User.Identity.IsAuthenticated)
            {
                await HttpContext.SignOutAsync(IdentityConstants.ApplicationScheme);
                return Redirect(Request.Path);
            }

            var model = new LoginModel();

            return View(model);
        }

        /// <summary>
        /// Process the user login.
        /// </summary>
        /// <param name="model"><see cref="LoginModel"/>.</param>
        /// <param name="returnUrl">The url to redirect if the login is successful.</param>
        [HttpPost, AntiForgery]
        public async Task<IActionResult> Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var isSignedIn = await SignInAsync(model);
                if (isSignedIn)
                {
                    return IsReturnUrlValidAfterLogin(returnUrl)
                        ? (IActionResult)Redirect(returnUrl)
                        : RedirectToRoute(Constants.Routes.ROOT);
                }
            }

            return View(model);
        }

        /// <summary>
        /// Process the user "logout"
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(IdentityConstants.ApplicationScheme);

            return RedirectToRoute(Constants.Routes.ROOT);
        }

        private async Task<bool> SignInAsync(LoginModel model)
        {
            var isSignedIn = false;
            var user = await _signInManager.UserManager.FindByEmailAsync(model.Email);

            if (user == null)
            {
                ModelState.AddModelError(string.Empty, "Email or password is invalid.");
            }
            else
            {
                var isPasswordValid = await _signInManager.CheckPasswordSignInAsync(user, model.Password, false);

                if (!isPasswordValid.Succeeded)
                {
                    ModelState.AddModelError(string.Empty, "Email or password is invalid.");
                }
                else
                {
                    await _signInManager.SignInAsync(user, false);
                    isSignedIn = true;
                }
            }

            return isSignedIn;
        }

        private bool IsReturnUrlValidAfterLogin(string returnUrl)
        {
            return returnUrl != null
                && Url.IsLocalUrl(returnUrl)
                && !Regex.IsMatch(returnUrl, @"(.*)\/login(\?|\/)?(.*)", RegexOptions.IgnoreCase);
        }
    }
}