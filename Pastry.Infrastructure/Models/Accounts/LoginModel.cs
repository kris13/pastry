﻿using System.ComponentModel.DataAnnotations;

namespace Pastry.Infrastructure.Models.Accounts
{
    /// <summary>
    /// Defines the model for the login.
    /// </summary>
    public sealed class LoginModel
    {
        /// <summary>
        /// Email used to login.
        /// </summary>
        [Display(Name = "Email")]
        [Required, DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        /// <summary>
        /// Password used to login.
        /// </summary>
        [Display(Name = "Password")]
        [Required, DataType(DataType.Password)]
        public string Password { get; set; }
    }
}