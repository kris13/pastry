﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pastry.Data.Entities.Products
{
    /// <summary>
    /// Defines the properties of an ingredient.
    /// </summary>
    public sealed class IngredientEntity
    {
        /// <summary>
        /// Id of the ingredient.
        /// </summary>
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Name of the ingredient.
        /// </summary>
        [Required, MaxLength(64)]
        public string Name { get; set; }
    }
}