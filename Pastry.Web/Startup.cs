using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Pastry.Implementation.Extensions;
using Pastry.Web.Extensions;
using Pastry.Web.Middlewares;
using Utils.Web.Binders.Providers;
using Utils.Web.Extensions;

namespace Pastry.Web
{
    /// <summary>
    /// Defines the startup methods for the application.
    /// </summary>
    /// <summary>
    /// Defines the startup methods for the application.
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Startup"/> class.
        /// </summary>
        public Startup()
        {
        }

        /// <summary>
        /// Configures the services of the application.
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/> used to configure the services.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddApplicationServices();

            services.AddAntiforgeryService();
            services.AddHttpContextAccessor();
            services.AddUrlHelper();

            services.AddAuthorizationServices();

            services.Configure<CookiePolicyOptions>(options => options.MinimumSameSitePolicy = SameSiteMode.Strict);

            services.AddControllersWithViews(options =>
            {
                options.EnableEndpointRouting = false;
                options.ModelBinderProviders.Insert(0, new NumberModelBinderProvider());
            });
        }

        /// <summary>
        /// Configure the HTTP request pipeline of the application.
        /// </summary>
        /// <param name="applicationBuilder"><see cref="IApplicationBuilder"/> used to configure the application's requests.</param>
        /// <param name="environment"><see cref="IWebHostEnvironment"/> used to determine the informations about the web hosting environment.</param>
        public void Configure(IApplicationBuilder applicationBuilder, IWebHostEnvironment environment)
        {
            applicationBuilder.UseMiddleware<ErrorMiddleware>();

            if (!environment.IsDevelopment())
            {
                applicationBuilder.UseHsts();
            }

            applicationBuilder.ConfigureAuthorization();

            applicationBuilder.UseHttpsRedirection();
            applicationBuilder.UseStaticFiles();
            applicationBuilder.UseCookiePolicy();

            applicationBuilder.UseMvc(routes =>
            {
                routes.MapRoute(Constants.Routes.ERROR, "error/{code:int}", new { controller = "Error", action = "Index" }, null);
                routes.MapRoute(Constants.Routes.Account.LOGIN, "login", new { controller = "Account", action = "Login" }, null);
                routes.MapRoute(Constants.Routes.Account.LOGOUT, "logout", new { controller = "Account", action = "Logout" }, null);
                routes.MapRoute(Constants.Routes.Shop.PRODUCTS, "", new { controller = "Shop", action = "Products" });
                routes.MapRoute(Constants.Routes.ProductManagement.PRODUCTS, "product-management", new { controller = "ProductManagement", action = "Products" });
                routes.MapRoute(Constants.Routes.ProductManagement.CREATE_PRODUCT, "product-management/create", new { controller = "ProductManagement", action = "Create" });
                routes.MapRoute(Constants.Routes.ProductManagement.EDIT_PRODUCT, "product-management/edit/{id:int}", new { controller = "ProductManagement", action = "Edit" });
                routes.MapRoute(Constants.Routes.ProductManagement.SAVE_PRODUCT, "product-management/save", new { controller = "ProductManagement", action = "Save" });
            });

            applicationBuilder.ApplicationServices.Initialize();
        }
    }
}