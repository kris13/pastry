﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace Utils.Exceptions
{
    /// <summary>
    /// The exception that is thrown when one of the argument provided  is not valid.
    /// </summary>
    public sealed class InvalidObjectException : Exception
    {
        /// <summary>
        /// The collection of the validation errors.
        /// </summary>
        public IReadOnlyCollection<ValidationResult> ValidationResults { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="InvalidObjectException"/> class.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="validationResults">The collection of the validation errors.</param>
        public InvalidObjectException(string message, IList<ValidationResult> validationResults)
            : base(message)
        {
            ValidationResults = new ReadOnlyCollection<ValidationResult>(validationResults);
        }

        /// <inheritdoc/>
        [Obsolete("Use the other constructor", true)]
        public InvalidObjectException()
        {
        }

        /// <inheritdoc/>
        [Obsolete("Use the other constructor", true)]
        public InvalidObjectException(string message)
            : base(message)
        {
        }

        /// <inheritdoc/>
        [Obsolete("Use the other constructor", true)]
        public InvalidObjectException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}