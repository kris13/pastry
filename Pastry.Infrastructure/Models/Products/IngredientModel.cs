﻿namespace Pastry.Infrastructure.Models.Products
{
    /// <summary>
    /// Defines the properties of an ingredient.
    /// </summary>
    public sealed class IngredientModel
    {
        /// <summary>
        /// Id of the ingredient.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Name of the ingredient.
        /// </summary>
        public string Name { get; set; }
    }
}