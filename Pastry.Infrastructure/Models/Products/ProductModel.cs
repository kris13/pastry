﻿using System;
using System.Collections.Generic;

namespace Pastry.Infrastructure.Models.Products
{
    /// <summary>
    /// Defines the properties of a product.
    /// </summary>
    public sealed class ProductModel : ISoldModel
    {
        /// <summary>
        /// Id of the product.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Name of the product.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Available quantity of the product.
        /// </summary>
        public uint AvailableQuantity { get; set; }

        /// <summary>
        /// Price  of the product.
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Date of sale of the product.
        /// </summary>
        public DateTime SaleDate { get; set; }

        /// <summary>
        /// Discounted price of the product.
        /// </summary>
        public decimal? DiscountedPrice { get; set; }

        /// <summary>
        /// Ingredients of the product.
        /// </summary>
        public ICollection<ProductIngredientModel> Ingredients { get; set; }
    }
}