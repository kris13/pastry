﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;

namespace Pastry.Web.Extensions
{
    /// <summary>
    /// Extension methods for <see cref="IApplicationBuilder"/>.
    /// </summary>
    internal static class IApplicationBuilderExtensions
    {
        /// <summary>
        /// Configures authorization services to the specified <see cref="IApplicationBuilder"/>.
        /// </summary>
        /// <param name="applicationBuilder">The <see cref="IApplicationBuilder"/> to configure.</param>
        /// <returns>The <see cref="IApplicationBuilder"/> so that additional calls can be chained.</returns>
        internal static IApplicationBuilder ConfigureAuthorization(this IApplicationBuilder applicationBuilder)
        {
            applicationBuilder.UseMiddleware<AuthenticationMiddleware>();

            return applicationBuilder;
        }
    }
}