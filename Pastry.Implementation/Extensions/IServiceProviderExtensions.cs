﻿using Microsoft.Extensions.DependencyInjection;
using Pastry.Data;
using Pastry.Data.Entities.Account;
using Pastry.Data.Entities.Products;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Pastry.Implementation.Extensions
{
    /// <summary>
    /// Extension methods for <see cref="IServiceProvider"/>.
    /// </summary>
    public static class IServiceProviderExtensions
    {
        /// <summary>
        /// Initializes the application.
        /// </summary>
        /// <param name="serviceProvider">The <see cref="IServiceProvider"/> to use.</param>
        /// <returns>The <paramref name="serviceProvider"/> so that additional calls can be chained.</returns>
        public static IServiceProvider Initialize(this IServiceProvider serviceProvider)
        {
            using (var scope = serviceProvider.CreateScope())
            {
                var dbContext = scope.ServiceProvider.GetRequiredService<PastryDbContext>();

                InitalizeUsers(dbContext);
                InitalizeDiscount(dbContext);
                InitalizeIngredients(dbContext);
                InitalizeProducts(dbContext);
            }

            return serviceProvider;
        }

        private static void InitalizeUsers(PastryDbContext dbContext)
        {
            //Password: Luana123!
            var luanaUser = new UserEntity() { Id = 1, UserName = "Luana", Email = "luana@pasticceria.it", PasswordHash = "AQAAAAEAACcQAAAAEMVJFb4pK6JxlnKjPSx0zhZ5HKPH6Ys1pU4lc1JWve35/cHRDjvW4HDXTG4XYMLM0g==" };
            //Password: Maria123!
            var mariaUser = new UserEntity() { Id = 2, UserName = "Maria", Email = "maria@pasticceria.it", PasswordHash = "AQAAAAEAACcQAAAAEGrUPOpG7WRXue4zZKd3tmBLVp7pXP7x04tlGUxps0MJU+w02/vqnnEx78PyBncMgA==" };

            dbContext.Users.AddRange(luanaUser, mariaUser);

            dbContext.SaveChanges();
        }

        private static void InitalizeDiscount(PastryDbContext dbContext)
        {
            var entities = new DiscountPriceEntity[] {
                new DiscountPriceEntity() { Id = 1, Days = 2, Percent = .2 },
                new DiscountPriceEntity() { Id = 2, Days = 3, Percent = .8 },
                new DiscountPriceEntity() { Id = 3, Days = 4, Percent = 1 }
            };

            dbContext.DiscountPrices.AddRange(entities);

            dbContext.SaveChanges();
        }

        private static void InitalizeIngredients(PastryDbContext dbContext)
        {
            var entities = new IngredientEntity[] {
                new IngredientEntity() { Id = 1, Name = "Uova" },
                new IngredientEntity() { Id = 2, Name = "Zucchero" },
                new IngredientEntity() { Id = 3, Name = "Mascarpone" },
                new IngredientEntity() { Id = 4, Name = "Farina" },
                new IngredientEntity() { Id = 5, Name = "Cacao amaro" },
                new IngredientEntity() { Id = 6, Name = "Lievito per dolci" },
                new IngredientEntity() { Id = 7, Name = "Olio di semi di girasole" },
                new IngredientEntity() { Id = 8, Name = "Cannella " },
                new IngredientEntity() { Id = 9, Name = "Olio di riso" },
                new IngredientEntity() { Id = 10, Name = "Latte di riso" },
                new IngredientEntity() { Id = 11, Name = "Farina di riso" },
                new IngredientEntity() { Id = 12, Name = "Scorza di limone" },
                new IngredientEntity() { Id = 13, Name = "Vaniglia" }
            };

            dbContext.Ingredients.AddRange(entities);

            dbContext.SaveChanges();
        }

        private static void InitalizeProducts(PastryDbContext dbContext)
        {
            var i = 1;
            var now = DateTime.Now.Date;
            var eIngredients = dbContext.Ingredients.ToList();

            //https://www.fattoincasadabenedetta.it/ricette-dolci-con-pochi-ingredienti/
            var entities = new ProductEntity[] {
                new ProductEntity()
                {
                    Id = 1,
                    Name = "TORTA SOFFICE MASCARPONE E CIOCCOLATO",
                    Price = 12m,
                    Quantity = 3,
                    SaleDate = now,
                    Ingredients = new List<ProductIngredientEntity>() {
                        new ProductIngredientEntity() { Id = i++, Ingredient = eIngredients[0], Quantity = 4, QuantityUnit = QuantityUnitEntity.Numero },
                        new ProductIngredientEntity() { Id = i++, Ingredient = eIngredients[1], Quantity = 200, QuantityUnit = QuantityUnitEntity.Grammi },
                        new ProductIngredientEntity() { Id = i++, Ingredient = eIngredients[2], Quantity = 250 , QuantityUnit = QuantityUnitEntity.Grammi },
                        new ProductIngredientEntity() { Id = i++, Ingredient = eIngredients[3], Quantity = 150 , QuantityUnit = QuantityUnitEntity.Grammi },
                        new ProductIngredientEntity() { Id = i++, Ingredient = eIngredients[4], Quantity = 50 , QuantityUnit = QuantityUnitEntity.Grammi },
                        new ProductIngredientEntity() { Id = i++, Ingredient = eIngredients[5], Quantity = 16, QuantityUnit = QuantityUnitEntity.Grammi }
                    }
                },
                new ProductEntity()
                {
                    Id = 2,
                    Name = "CROSTATA DI MARMELLATA",
                    Price = 18.50m,
                    Quantity = 2,
                    SaleDate = now.AddDays(-3),
                    Ingredients = new List<ProductIngredientEntity>() {
                        new ProductIngredientEntity() { Id = i++, Ingredient = eIngredients[0], Quantity = 2, QuantityUnit = QuantityUnitEntity.Numero },
                        new ProductIngredientEntity() { Id = i++, Ingredient = eIngredients[1], Quantity = 100, QuantityUnit = QuantityUnitEntity.Grammi },
                        new ProductIngredientEntity() { Id = i++, Ingredient = eIngredients[6], Quantity = 80  , QuantityUnit = QuantityUnitEntity.Grammi },
                        new ProductIngredientEntity() { Id = i++, Ingredient = eIngredients[3], Quantity = 300 , QuantityUnit = QuantityUnitEntity.Grammi },
                        new ProductIngredientEntity() { Id = i++, Ingredient = eIngredients[4], Quantity = 50 , QuantityUnit = QuantityUnitEntity.Grammi },
                        new ProductIngredientEntity() { Id = i++, Ingredient = eIngredients[11], Quantity = null }
                    }
                }
            };

            dbContext.Products.AddRange(entities);

            dbContext.SaveChanges();
        }
    }
}