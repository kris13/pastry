﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pastry.Data.Entities.Products
{
    /// <summary>
    /// Defines the properties of an ingredient for a product.
    /// </summary>
    public sealed class ProductIngredientEntity
    {
        /// <summary>
        /// Id of the product-ingredient.
        /// </summary>
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// The ingredient.
        /// </summary>
        public IngredientEntity Ingredient { get; set; }

        /// <summary>
        /// Quantity  of the ingredient.
        /// </summary>
        public double? Quantity { get; set; }

        /// <summary>
        /// Quantity unit of the ingredient.
        /// </summary>
        public QuantityUnitEntity QuantityUnit { get; set; }
    }
}