﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Pastry.Data;
using Pastry.Implementation.Helpers;
using Pastry.Infrastructure.Models.Accounts;
using Pastry.Infrastructure.Services.Accounts;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Pastry.Implementation.Services.Accounts
{
    /// <inheritdoc/>
    public sealed class UserStoreService : IUserStoreService
    {
        private readonly PastryDbContext _dbContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserStoreService"/> class.
        /// </summary>
        /// <param name="dbContext">Database context used to manage the users.</param>
        public UserStoreService(PastryDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        /// <inheritdoc/>
        public void Dispose()
        {
        }

        #region IUserStore

        /// <inheritdoc/>
        public Task<IdentityResult> CreateAsync(UserModel user, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc/>
        public Task<IdentityResult> DeleteAsync(UserModel user, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc/>
        public async Task<UserModel> FindByIdAsync(string userId, CancellationToken cancellationToken)
        {
            UserModel model = null;

            if (int.TryParse(userId, out var id))
            {
                var entity = await _dbContext.Users
                     .FindAsync(id);

                if (entity != null)
                {
                    model = AccountHelper.MapToModel(entity);
                }
            }

            return model;
        }

        /// <inheritdoc/>
        public async Task<UserModel> FindByNameAsync(string normalizedUserName, CancellationToken cancellationToken)
        {
            var entity = await _dbContext.Users
                .FirstOrDefaultAsync(e => e.UserName.Equals(normalizedUserName, StringComparison.InvariantCultureIgnoreCase));

            return entity != null ? AccountHelper.MapToModel(entity) : null;
        }

        /// <inheritdoc/>
        public Task<string> GetNormalizedUserNameAsync(UserModel user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.UserName);
        }

        /// <inheritdoc/>
        public Task<string> GetUserIdAsync(UserModel user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.Id.ToString());
        }

        /// <inheritdoc/>
        public Task<string> GetUserNameAsync(UserModel user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.UserName);
        }

        /// <inheritdoc/>
        public Task SetNormalizedUserNameAsync(UserModel user, string normalizedName, CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        /// <inheritdoc/>
        public Task SetUserNameAsync(UserModel user, string userName, CancellationToken cancellationToken)
        {
            user.UserName = userName;

            return Task.CompletedTask;
        }

        /// <inheritdoc/>
        public Task<IdentityResult> UpdateAsync(UserModel user, CancellationToken cancellationToken)
        {
            return Task.FromResult(IdentityResult.Success);
        }

        #endregion IUserStore

        #region IUserPasswordStore

        /// <inheritdoc/>
        public async Task SetPasswordHashAsync(UserModel user, string passwordHash, CancellationToken cancellationToken)
        {
            var entity = await _dbContext.Users.FindAsync(user.Id);

            if (entity != null)
            {
                entity.PasswordHash = passwordHash;
                await _dbContext.SaveChangesAsync();
            }
        }

        /// <inheritdoc/>
        public Task<bool> HasPasswordAsync(UserModel user, CancellationToken cancellationToken)
        {
            return Task.FromResult(true);
        }

        /// <inheritdoc/>
        public async Task<string> GetPasswordHashAsync(UserModel user, CancellationToken cancellationToken)
        {
            var entity = await _dbContext.Users.FindAsync(user.Id);

            return entity?.PasswordHash;
        }

        #endregion IUserPasswordStore

        #region IUserEmailStore

        /// <inheritdoc/>
        public Task SetEmailAsync(UserModel user, string email, CancellationToken cancellationToken)
        {
            user.Email = email;

            return Task.CompletedTask;
        }

        /// <inheritdoc/>
        public Task<string> GetEmailAsync(UserModel user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.Email);
        }

        /// <inheritdoc/>
        public Task<bool> GetEmailConfirmedAsync(UserModel user, CancellationToken cancellationToken)
        {
            return Task.FromResult(true);
        }

        /// <inheritdoc/>
        public Task SetEmailConfirmedAsync(UserModel user, bool confirmed, CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        /// <inheritdoc/>
        public async Task<UserModel> FindByEmailAsync(string normalizedEmail, CancellationToken cancellationToken)
        {
            var entity = await _dbContext.Users
                .FirstOrDefaultAsync(e => e.Email.Equals(normalizedEmail, StringComparison.InvariantCultureIgnoreCase));

            return entity != null ? AccountHelper.MapToModel(entity) : null;
        }

        /// <inheritdoc/>
        public Task<string> GetNormalizedEmailAsync(UserModel user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.Email);
        }

        /// <inheritdoc/>
        public Task SetNormalizedEmailAsync(UserModel user, string normalizedEmail, CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        #endregion IUserEmailStore
    }
}