﻿using Pastry.Data.Entities.Products;
using Pastry.Infrastructure.Models.Products;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Pastry.Implementation.Helpers
{
    internal static class ProductHelper
    {
        public static ICollection<ProductModel> MapToModels(IEnumerable<ProductEntity> entities, Action<ISoldModel> adjustAvailability)
        {
            return entities
                .Select(e => MapToModel(e, adjustAvailability))
                .ToList();
        }

        public static ICollection<ProductItemModel> MapToItemModels(IEnumerable<ProductEntity> entities, Action<ISoldModel> adjustAvailability)
        {
            return entities
                .Select(e => MapToItemModel(e, adjustAvailability))
                .ToList();
        }

        public static ICollection<IngredientModel> MapToModels(IEnumerable<IngredientEntity> entities)
        {
            return entities
                .Select(MapToModel)
                .ToList();
        }

        public static ProductCrudModel MapToModel(ProductEntity entity)
        {
            return new ProductCrudModel()
            {
                Id = entity.Id,
                Name = entity.Name,
                Quantity = entity.Quantity,
                Price = entity.Price,
                SaleDate = entity.SaleDate,
                Ingredients = MapToModels(entity.Ingredients)
            };
        }

        public static void MapToEntity(ProductCrudModel model, ProductEntity entity)
        {
            entity.Name = model.Name;
            entity.Quantity = model.Quantity;
            entity.Price = model.Price;
            entity.SaleDate = model.SaleDate;
        }

        public static ProductEntity MapToEntity(ProductCrudModel model)
        {
            var entity = new ProductEntity()
            {
                Id = new Random((int)DateTime.UtcNow.Ticks).Next(1, int.MaxValue),
                Ingredients = new List<ProductIngredientEntity>()
            };

            MapToEntity(model, entity);

            return entity;
        }

        private static ProductModel MapToModel(ProductEntity entity, Action<ISoldModel> adjustAvailability)
        {
            var model = new ProductModel()
            {
                Id = entity.Id,
                Name = entity.Name,
                AvailableQuantity = entity.Quantity,
                Price = entity.Price,
                SaleDate = entity.SaleDate,
                DiscountedPrice = null,
                Ingredients = MapToModels(entity.Ingredients)
            };

            adjustAvailability.Invoke(model);

            return model;
        }

        private static ProductItemModel MapToItemModel(ProductEntity entity, Action<ISoldModel> adjustAvailability)
        {
            var model = new ProductItemModel()
            {
                Id = entity.Id,
                Name = entity.Name,
                Quantity = entity.Quantity,
                AvailableQuantity = entity.Quantity,
                Price = entity.Price,
                SaleDate = entity.SaleDate,
                DiscountedPrice = null
            };

            adjustAvailability.Invoke(model);

            return model;
        }

        private static ICollection<ProductIngredientModel> MapToModels(IEnumerable<ProductIngredientEntity> entities)
        {
            return entities
                .Select(MapToModel)
                .ToList();
        }

        private static ProductIngredientModel MapToModel(ProductIngredientEntity entity)
        {
            return new ProductIngredientModel()
            {
                Ingredient = MapToModel(entity.Ingredient),
                Quantity = entity.Quantity,
                QuantityUnit = (QuantityUnitModel)entity.QuantityUnit
            };
        }

        private static IngredientModel MapToModel(IngredientEntity entity)
        {
            return new IngredientModel()
            {
                Id = entity.Id,
                Name = entity.Name
            };
        }
    }
}