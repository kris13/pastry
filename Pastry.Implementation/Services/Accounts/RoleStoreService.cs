﻿using Microsoft.AspNetCore.Identity;
using Pastry.Infrastructure.Models.Accounts;
using Pastry.Infrastructure.Services.Accounts;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Pastry.Implementation.Services.Accounts
{
    /// <inheritdoc/>
    public sealed class RoleStoreService : IRoleStoreService
    {
        /// <inheritdoc/>
        public Task<IdentityResult> CreateAsync(RoleModel role, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc/>
        public Task<IdentityResult> DeleteAsync(RoleModel role, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc/>
        public void Dispose()
        {
        }

        /// <inheritdoc/>
        public Task<RoleModel> FindByIdAsync(string roleId, CancellationToken cancellationToken)
        {
            return Task.FromResult<RoleModel>(null);
        }

        /// <inheritdoc/>
        public Task<RoleModel> FindByNameAsync(string normalizedRoleName, CancellationToken cancellationToken)
        {
            return Task.FromResult<RoleModel>(null);
        }

        /// <inheritdoc/>
        public Task<string> GetNormalizedRoleNameAsync(RoleModel role, CancellationToken cancellationToken)
        {
            return Task.FromResult(role.Name);
        }

        /// <inheritdoc/>
        public Task<string> GetRoleIdAsync(RoleModel role, CancellationToken cancellationToken)
        {
            return Task.FromResult(role.Id.ToString());
        }

        /// <inheritdoc/>
        public Task<string> GetRoleNameAsync(RoleModel role, CancellationToken cancellationToken)
        {
            return Task.FromResult(role.Name);
        }

        /// <inheritdoc/>
        public Task SetNormalizedRoleNameAsync(RoleModel role, string normalizedName, CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        /// <inheritdoc/>
        public Task SetRoleNameAsync(RoleModel role, string roleName, CancellationToken cancellationToken)
        {
            role.Name = roleName;

            return Task.CompletedTask;
        }

        /// <inheritdoc/>
        public Task<IdentityResult> UpdateAsync(RoleModel role, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}