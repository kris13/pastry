﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System.Net;
using System.Threading.Tasks;
using Utils.Guards;
using Utils.Web.Features;

namespace Utils.Web.Helpers
{
    /// <summary>
    /// Helper to re-execute an action.
    /// </summary>
    public static class ReExecuteHelper
    {
        /// <summary>
        /// Gets the <see cref="HttpStatusCode"/> standardized.
        /// </summary>
        /// <param name="response">The <see cref="HttpResponse"/> for which the status code must be standardized.</param>
        /// <returns>The <see cref="HttpResponse.StatusCode"/> standardized.</returns>
        /// <remarks>
        /// <list type="bullet">
        /// <listheader>Rules for the standardization.</listheader>
        /// <item>The <see cref="HttpStatusCode.Unauthorized"/> does not change.</item>
        /// <item>The <see cref="HttpStatusCode.Forbidden"/> does not change.</item>
        /// <item>The <see cref="HttpStatusCode.NotFound"/> does not change.</item>
        /// <item>The 440 (Expired session) does not change.</item>
        /// <item>The <see cref="HttpStatusCode.PreconditionRequired"/> value does not change.</item>
        /// <item>The <see cref="HttpResponse.StatusCode"/> with a value greater than or equal to 500 become <see cref="HttpStatusCode.InternalServerError"/>.</item>
        /// <item>The <see cref="HttpResponse.StatusCode"/> with a value between 400 and 500 become <see cref="HttpStatusCode.BadRequest"/>.</item>
        /// </list>
        /// </remarks>
        public static HttpStatusCode GetStatusCodeResponseStandardized(HttpResponse response)
        {
            var statusCode = HttpStatusCode.OK;

            if (response != null)
            {
                if (response.StatusCode == (int)HttpStatusCode.Unauthorized
                    || response.StatusCode == (int)HttpStatusCode.Forbidden
                    || response.StatusCode == (int)HttpStatusCode.NotFound)
                {
                    statusCode = (HttpStatusCode)response.StatusCode;
                }
                else if (response.StatusCode >= 500)
                {
                    statusCode = HttpStatusCode.InternalServerError;
                }
                else if (response.StatusCode >= 400)
                {
                    statusCode = HttpStatusCode.BadRequest;
                }
            }

            return statusCode;
        }

        /// <summary>
        /// Generate the response body by re-executing the request pipeline using an alternate path.
        /// </summary>
        /// <param name="httpContext">The <see cref="HttpContext"/> for the current request.</param>
        /// <param name="next">The delegate representing the remaining middleware in the request pipeline.</param>
        /// <param name="statusCode">The <see cref="HttpStatusCode"/> of the re-excute response.</param>
        /// <param name="pathToReExecute">The path to re-execute. It must begin by the '/' character.</param>
        public static async Task ReExecutePath(HttpContext httpContext, RequestDelegate next, HttpStatusCode statusCode, string pathToReExecute)
        {
            Guard.NotNull(nameof(httpContext), httpContext);
            Guard.NotNull(nameof(next), next);
            Guard.NotNull(nameof(pathToReExecute), pathToReExecute);

            if (pathToReExecute != null)
            {
                httpContext.Response.Clear();
                httpContext.Response.StatusCode = (int)statusCode;
                ClearCacheHeaders(httpContext.Response);

                var originalPath = httpContext.Request.Path;
                var originalQueryString = httpContext.Request.QueryString;

                httpContext.Features.Set(new ReExecuteFeature(httpContext));

                httpContext.Request.Path = pathToReExecute;
                httpContext.Request.QueryString = QueryString.Empty;

                try
                {
                    await next(httpContext);
                }
                finally
                {
                    httpContext.Request.QueryString = originalQueryString;
                    httpContext.Request.Path = originalPath;
                    httpContext.Features.Set<ReExecuteFeature>(null);
                }
            }
        }

        /// <summary>
        /// Generate the response body by re-executing the request pipeline using a route name.
        /// </summary>
        /// <param name="httpContext">The <see cref="HttpContext"/> for the current request.</param>
        /// <param name="next">The delegate representing the remaining middleware in the request pipeline.</param>
        /// <param name="statusCode">The <see cref="HttpStatusCode"/> of the re-excute response.</param>
        /// <param name="routeName">The route name to re-execute.</param>
        /// <param name="values">An object that contains route values.</param>
        public static async Task ReExecuteRoute(HttpContext httpContext, RequestDelegate next, HttpStatusCode statusCode, string routeName, object values = null)
        {
            Guard.NotNull(nameof(httpContext), httpContext);
            Guard.NotNull(nameof(next), next);
            Guard.NotNull(nameof(routeName), routeName);

            var pathToReExecute = GetRoutePath(httpContext, routeName, values);

            await ReExecutePath(httpContext, next, statusCode, pathToReExecute);
        }

        /// <summary>
        /// Generates a URL for the specified routeName.
        /// </summary>
        /// <param name="httpContext">The <see cref="HttpContext"/> for the current request.</param>
        /// <param name="routeName">The name of the route that is used to generate URL.</param>
        /// <param name="values">An object that contains route values.</param>
        /// <returns>The generated URL.</returns>
        public static string GetRoutePath(HttpContext httpContext, string routeName, object values = null)
        {
            var urlHelper = httpContext.RequestServices.GetRequiredService<IUrlHelper>();
            return urlHelper.RouteUrl(routeName, values);
        }

        private static void ClearCacheHeaders(HttpResponse response)
        {
            response.Headers["Cache-Control"] = "no-cache";
            response.Headers["Pragma"] = "no-cache";
            response.Headers["Expires"] = "-1";
            response.Headers.Remove("ETag");
        }
    }
}