﻿namespace Pastry.Infrastructure.Models.Products
{
    /// <summary>
    /// Defines the quantity unit for an ingredient.
    /// </summary>
    public enum QuantityUnitModel
    {
        /// <summary>
        /// Number of.
        /// </summary>
        Number,

        /// <summary>
        /// Gram unit.
        /// </summary>
        Gram,

        /// <summary>
        /// Liter unit.
        /// </summary>
        Liter,

        /// <summary>
        /// Teaspoon unit.
        /// </summary>
        Teaspoon
    }
}