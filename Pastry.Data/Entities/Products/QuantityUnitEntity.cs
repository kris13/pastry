﻿namespace Pastry.Data.Entities.Products
{
    /// <summary>
    /// Defines the quantity unit for an ingredient.
    /// </summary>
    public enum QuantityUnitEntity
    {
        Numero,

        Grammi,

        Litri,

        Teaspoon
    }
}