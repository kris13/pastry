﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Pastry.Infrastructure.Models.Products;
using Pastry.Infrastructure.Services.Products;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utils.Web.Attributes;

namespace Pastry.Web.Areas.Cms.Controllers
{
    /// <summary>
    /// <see cref="Controller"/> used to manage the products.
    /// </summary>
    [Authorize]
    public sealed class ProductManagementController : Controller
    {
        /// <summary>
        /// Gets the HTML page for displaying the products.
        /// </summary>
        /// <param name="productService">The <see cref="IProductService"/> used to retreive the products.</param>
        /// <returns>The HTML page for displaying the products.</returns>
        [HttpGet]
        public async Task<ViewResult> Products([FromServices] IProductService productService)
        {
            var models = await productService.GetAllItemsAsync();

            return View(models);
        }

        /// <summary>
        /// Gets the HTML page for editing a product.
        /// </summary>
        /// <param name="id">Id product to look for.</param>
        /// <param name="productService">The <see cref="IProductService"/> used to retreive the products.</param>
        /// <returns>The HTML page for editing the product found.</returns>
        [HttpGet]
        public async Task<IActionResult> Edit(int id, [FromServices] IProductService productService)
        {
            var model = await productService.FindByIdAsync(id);

            if (model == null)
            {
                return NotFound();
            }

            ViewBag.Ingredients = await productService.GetAllIngredientsAsync();

            return View(model);
        }

        /// <summary>
        /// Gets the HTML page for creating a product.
        /// </summary>
        /// <returns>The HTML page for editing the product found.</returns>
        [HttpGet]
        public async Task<IActionResult> Create([FromServices] IProductService productService)
        {
            var model = new ProductCrudModel()
            {
                SaleDate = DateTime.Now,
                Ingredients = new List<ProductIngredientModel>()
            };

            ViewBag.Ingredients = await productService.GetAllIngredientsAsync();

            return View(nameof(Edit), model);
        }

        /// <summary>
        /// Saves a product and back to the products page.
        /// </summary>
        [HttpPost, AntiForgery]
        public async Task<IActionResult> Save(ProductCrudModel model, [FromServices] IProductService productService)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Ingredients = await productService.GetAllIngredientsAsync();

                return View(nameof(Edit), model);
            }

            if (model.Id > 0)
            {
                await productService.UpdateAsync(model);
            }
            else
            {
                await productService.CreateAsync(model);
            }

            return RedirectToRoute(Constants.Routes.ProductManagement.PRODUCTS);
        }
    }
}