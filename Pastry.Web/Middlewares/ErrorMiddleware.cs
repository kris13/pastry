﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.Extensions.Logging;
using System;
using System.Net;
using System.Threading.Tasks;
using Utils.Web.Helpers;

namespace Pastry.Web.Middlewares
{
    /// <summary>
    /// Middleware that manage the http unauthorized error (401).
    /// It catches the http unauthorized error (401) and redirects to the login page if the <see cref="HttpResponse.StatusCode"/> is not equal to <see cref="HttpStatusCode.OK"/>.
    /// </summary>
    public sealed class ErrorMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnauthorizedErrorMiddleware"/> class.
        /// </summary>
        /// <param name="next">The delegate representing the next middleware in the request pipeline.</param>
        /// <param name="logger"><see cref="ILogger"/> to use.</param>
        public ErrorMiddleware(RequestDelegate next, ILogger<ErrorMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        /// <summary>
        /// Executes the middleware.
        /// </summary>
        /// <param name="httpContext">The <see cref="HttpContext"/> for the current request.</param>
        /// <returns>A task that represents the execution of this middleware.</returns>
        public async Task Invoke(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);

                if (!httpContext.Response.HasStarted && !httpContext.Response.ContentLength.HasValue)
                {
                    var statusCodeToProcess = ReExecuteHelper.GetStatusCodeResponseStandardized(httpContext.Response);
                    if (statusCodeToProcess == HttpStatusCode.Unauthorized)
                    {
                        await ReExecuteHelper.ReExecuteRoute(httpContext, _next, HttpStatusCode.Unauthorized, Constants.Routes.Account.LOGIN);
                    }
                    else if (statusCodeToProcess != HttpStatusCode.OK)
                    {
                        await ReExecuteHelper.ReExecuteRoute(httpContext, _next, statusCodeToProcess, Constants.Routes.ERROR, new { code = (int)statusCodeToProcess });
                    }
                }
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "", new { url = httpContext?.Request?.GetDisplayUrl() ?? "no url" });

                if (httpContext.Response.HasStarted)
                {
                    throw;
                }

                try
                {
                    await ReExecuteHelper.ReExecuteRoute(httpContext, _next, HttpStatusCode.InternalServerError, Constants.Routes.ERROR, new { code = (int)HttpStatusCode.InternalServerError });
                }
                catch (Exception subException)
                {
                    _logger.LogCritical(exception, subException.Message, new { url = httpContext?.Request?.GetDisplayUrl() ?? "no url" });
                    throw;
                }
            }
        }
    }
}