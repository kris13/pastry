DECLARE @dataSinistri DATE =  CONVERT(DATE, '13/12/2019', 103);
DECLARE @assicurazioni NVARCHAR(50) =  'SARA';
DECLARE @marca NVARCHAR(50) =  'FIAT';
DECLARE @cilindrataMin INT = 2000;
DECLARE @potenzaMin INT = 120;

-- 1) Targa e Marca delle Auto di cilindrata superiore a 2000 cc o di potenza superiore a 120 CV

SELECT Targa
	 , Marca
FROM [AUTO]
WHERE (Cilindrata > @cilindrataMin OR Potenza > @potenzaMin)

-- 2) Nome del proprietario e Targa delle Auto di cilindrata superiore a 2000 cc oppure di potenza superiore a 120 CV
SELECT PROPRIETARI.Nome
FROM PROPRIETARI
	INNER JOIN [AUTO] ON [AUTO].CodF = PROPRIETARI.CodF
WHERE ([AUTO].Cilindrata > @cilindrataMin OR [AUTO].Potenza > @potenzaMin)

-- 3) Targa e Nome del proprietario delle Auto di cilindrata superiore a 2000 cc oppure di potenza superiore a 120 CV, assicurate presso la �SARA�
SELECT [AUTO].Targa
	 , PROPRIETARI.Nome AS NomeProprietario
FROM PROPRIETARI
	INNER JOIN [AUTO] ON [AUTO].CodF = PROPRIETARI.CodF
	INNER JOIN ASSICURAZIONI ON ASSICURAZIONI.CodAss = [AUTO].CodAss
WHERE ASSICURAZIONI.Nome = @assicurazioni AND ([AUTO].Cilindrata > @cilindrataMin OR [AUTO].Potenza > @potenzaMin)

-- 4) Targa e Nome del proprietario delle Auto assicurate presso la �SARA� e coinvolte in sinistri il 20/01/02
SELECT [AUTO].Targa
	 , PROPRIETARI.Nome AS NomeProprietario
FROM SINISTRI
	INNER JOIN AUTOCOINVOLTE ON AUTOCOINVOLTE.CodS = SINISTRI.CodS
	INNER JOIN [AUTO] ON [AUTO].Targa = AUTOCOINVOLTE.Targa
	INNER JOIN PROPRIETARI ON PROPRIETARI.CodF = [AUTO].CodF
	INNER JOIN ASSICURAZIONI ON ASSICURAZIONI.CodAss = [AUTO].CodAss
WHERE ASSICURAZIONI.Nome = @assicurazioni AND SINISTRI.[Data] = @dataSinistri

-- 5) Per ciascuna Assicurazione, il nome, la sede ed il numero di auto assicurate
SELECT ASSICURAZIONI.Nome
     , ASSICURAZIONI.Sede
	 , COUNT(*) AS NumeroAuto
FROM ASSICURAZIONI
	LEFT JOIN [AUTO] ON [AUTO].CodAss = ASSICURAZIONI.CodAss
GROUP BY ASSICURAZIONI.Nome, ASSICURAZIONI.Sede

-- 6) Per ciascuna auto �Fiat�, la targa dell�auto ed il numero di sinistri in cui � stata coinvolta
SELECT [AUTO].Targa
     , COUNT(*) AS NumeroSinitri
FROM [AUTO]
	LEFT JOIN AUTOCOINVOLTE ON AUTOCOINVOLTE.Targa = [AUTO].Targa
WHERE [AUTO].Marca = @marca
GROUP BY [AUTO].Targa

-- 7) Per ciascuna auto coinvolta in pi� di un sinistro, la targa dell�auto, il nome dell�Assicurazione, ed il totale dei danni riportati
SELECT [AUTO].Targa
     , ASSICURAZIONI.Nome AS NomeAssicurazione
     , COUNT(*) AS NumeroSinitri
FROM [AUTO]
	INNER JOIN ASSICURAZIONI ON ASSICURAZIONI.CodAss = [AUTO].CodAss
	LEFT JOIN AUTOCOINVOLTE ON AUTOCOINVOLTE.Targa = [AUTO].Targa
GROUP BY [AUTO].Targa, ASSICURAZIONI.Nome
HAVING COUNT(*) > 1

-- 8) CodF e Nome di coloro che possiedono pi� di un�auto
SELECT PROPRIETARI.CodF
     , PROPRIETARI.Nome
FROM PROPRIETARI
	INNER JOIN [AUTO] ON [AUTO].CodF = PROPRIETARI.CodF
GROUP BY PROPRIETARI.CodF, PROPRIETARI.Nome
HAVING COUNT(*) > 1

-- 9) La targa delle auto che non sono state coinvolte in sinistri dopo il 20/01/01



-- 10) Il codice dei sinistri in cui non sono state coinvolte auto con cilindrata inferiore a 2000 cc

