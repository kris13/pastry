﻿namespace Pastry.Infrastructure.Models.Products
{
    /// <summary>
    /// Defines the properties of an ingredient for a product.
    /// </summary>
    public sealed class ProductIngredientModel
    {
        /// <summary>
        /// The ingredient.
        /// </summary>
        public IngredientModel Ingredient { get; set; }

        /// <summary>
        /// Quantity  of the ingredient.
        /// </summary>
        public double? Quantity { get; set; }

        /// <summary>
        /// Quantity unit of the ingredient.
        /// </summary>
        public QuantityUnitModel QuantityUnit { get; set; }
    }
}