﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pastry.Data.Entities.Products
{
    /// <summary>
    /// Defines the properties of a product.
    /// </summary>
    public sealed class ProductEntity
    {
        /// <summary>
        /// Id of the product.
        /// </summary>
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Name of the product.
        /// </summary>
        [Required, MaxLength(256)]
        public string Name { get; set; }

        /// <summary>
        /// Quantity of the product.
        /// </summary>
        public uint Quantity { get; set; }

        /// <summary>
        /// Price  of the product.
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Date of sale of the product.
        /// </summary>
        public DateTime SaleDate { get; set; }

        /// <summary>
        /// Ingredients of the product.
        /// </summary>
        public ICollection<ProductIngredientEntity> Ingredients { get; set; }
    }
}