﻿using Microsoft.AspNetCore.Identity;
using Pastry.Infrastructure.Models.Accounts;

namespace Pastry.Infrastructure.Services.Accounts
{
    /// <summary>
    /// Provides the methods to manage the users.
    /// </summary>
    public interface IUserStoreService : IUserPasswordStore<UserModel>, IUserEmailStore<UserModel>
    {
    }
}