﻿using Microsoft.AspNetCore.Mvc;
using Pastry.Infrastructure.Services.Products;
using System.Threading.Tasks;

namespace Pastry.Web.Controllers
{
    /// <summary>
    /// <see cref="Controller"/> used to view the shop.
    /// </summary>
    public sealed class ShopController : Controller
    {
        /// <summary>
        /// Gets the HTML page for displaying the products.
        /// </summary>
        /// <param name="productService">The <see cref="IProductService"/> used to retreive the products.</param>
        /// <returns>The HTML page for displaying the products.</returns>
        [HttpGet]
        public async Task<ViewResult> Products([FromServices] IProductService productService)
        {
            var models = await productService.GetAllAsync();

            return View(models);
        }
    }
}