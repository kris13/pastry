﻿namespace Pastry.Data.Entities.Products
{
    /// <summary>
    /// Defines the properties of a discount applied to a product.
    /// </summary>
    public sealed class DiscountPriceEntity
    {
        /// <summary>
        /// Id of the discount.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Days to apply the discount.
        /// </summary>
        public int Days { get; set; }

        /// <summary>
        /// Percent of the discount.
        /// </summary>
        public double Percent { get; set; }
    }
}