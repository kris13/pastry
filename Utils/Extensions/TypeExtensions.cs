﻿using System;

namespace Utils.Extensions
{
    /// <summary>
    /// Extension methods for <see cref="Type"/>.
    /// </summary>
    public static class TypeExtensions
    {
        /// <summary>
        /// Indicates if given type can be equal to null.
        /// </summary>
        /// <param name="this">The @this to act on.</param>
        /// <returns>True if the <paramref name="this"/> can be equal to null; otherwise false.</returns>
        public static bool IsNullable(this Type @this)
        {
            return !@this.IsValueType || (@this.IsGenericType && @this.GetGenericTypeDefinition() == typeof(Nullable<>));
        }
    }
}