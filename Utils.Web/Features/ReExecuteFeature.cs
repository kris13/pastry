﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Utils.Web.Helpers;

namespace Utils.Web.Features
{
    /// <summary>
    /// Request feature used by <see cref="ReExecuteHelper"/>.
    /// This feature is added when <see cref="ReExecuteHelper"/> methods are called.
    /// </summary>
    public sealed class ReExecuteFeature
    {
        /// <summary>
        /// Orginal <see cref="HttpRequest.Path"/>.
        /// </summary>
        public string OriginalPath { get; }

        /// <summary>
        /// Orginal <see cref="HttpRequest.PathBase"/>.
        /// </summary>
        public string OriginalPathBase { get; }

        /// <summary>
        /// Orginal <see cref="HttpRequest.QueryString"/>.
        /// </summary>
        public string OriginalQueryString { get; }

        /// <summary>
        /// Original <see cref="RouteData"/> (set from <see cref="HttpContext"/>).
        /// </summary>
        public RouteData OriginalRouteData { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReExecuteFeature"/> class.
        /// </summary>
        /// <param name="httpContext"><see cref="HttpContext"/> used to initialize the properties.</param>
        public ReExecuteFeature(HttpContext httpContext)
        {
            var originalQueryString = httpContext.Request.QueryString;

            OriginalPathBase = httpContext.Request.PathBase.Value;
            OriginalPath = httpContext.Request.Path;
            OriginalQueryString = originalQueryString.HasValue ? originalQueryString.Value : null;
            OriginalRouteData = httpContext.GetRouteData();
        }
    }
}