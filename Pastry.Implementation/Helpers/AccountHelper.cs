﻿using Pastry.Data.Entities.Account;
using Pastry.Infrastructure.Models.Accounts;

namespace Pastry.Implementation.Helpers
{
    internal static class AccountHelper
    {
        public static UserModel MapToModel(UserEntity entity)
        {
            return new UserModel()
            {
                Id = entity.Id,
                UserName = entity.UserName,
                Email = entity.Email
            };
        }
    }
}