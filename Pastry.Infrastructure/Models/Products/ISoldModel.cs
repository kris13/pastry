﻿using System;

namespace Pastry.Infrastructure.Models.Products
{
    /// <summary>
    /// Properties used by a class with a sale implementation.
    /// </summary>
    public interface ISoldModel
    {
        /// <summary>
        /// Quantity available for the sale.
        /// </summary>
        uint AvailableQuantity { get; set; }

        /// <summary>
        /// Sale price.
        /// </summary>
        decimal Price { get; set; }

        /// <summary>
        /// Sale date.
        /// </summary>
        DateTime SaleDate { get; set; }

        /// <summary>
        /// Discounted price.
        /// </summary>
        decimal? DiscountedPrice { get; set; }
    }
}