﻿using Pastry.Infrastructure.Models.Products;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Pastry.Infrastructure.Services.Products
{
    /// <summary>
    /// Provides the methods to manage the products.
    /// </summary>
    public interface IProductService
    {
        /// <summary>
        /// Gets the collection of all <see cref="ProductModel"/>.
        /// </summary>
        /// <returns>A collection of <see cref="ProductModel"/> containing all products.</returns>
        Task<ICollection<ProductModel>> GetAllAsync();

        /// <summary>
        /// Gets the collection of all <see cref="ProductItemModel"/>.
        /// </summary>
        /// <returns>A collection of <see cref="ProductItemModel"/> containing all products.</returns>
        Task<ICollection<ProductItemModel>> GetAllItemsAsync();

        /// <summary>
        /// Gets the collection of all <see cref="IngredientModel"/>.
        /// </summary>
        /// <returns>A collection of <see cref="IngredientModel"/> containing all ingredients.</returns>
        Task<ICollection<IngredientModel>> GetAllIngredientsAsync();

        /// <summary>
        /// Find a product.
        /// </summary>
        /// <param name="id">Id product to look for.</param>
        /// <returns>An instance of <see cref="ProductModel"/> if it exists, otherwise null.</returns>
        Task<ProductCrudModel> FindByIdAsync(int id);

        /// <summary>
        /// Deletes a product.
        /// </summary>
        /// <param name="id">Id product to look for.</param>
        Task DeleteAsync(int id);

        /// <summary>
        /// Creates a product.
        /// </summary>
        /// <param name="model">The <see cref="ProductCrudModel"/> to create.</param>
        /// <returns>True if the product is created successfully, otherwise false.</returns>
        Task<bool> CreateAsync(ProductCrudModel model);

        /// <summary>
        /// Updates a product.
        /// </summary>
        /// <param name="model">The <see cref="ProductCrudModel"/> to update.</param>
        /// <returns>True if the product is updated successfully, otherwise false.</returns>
        Task<bool> UpdateAsync(ProductCrudModel model);
    }
}